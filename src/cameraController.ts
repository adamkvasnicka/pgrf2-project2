import { Camera } from "./Camera"
import { STEP } from "./utils/constants"

/**
 * Simple controller class for our camera using event listeners
 * 
 * @export
 * @class cameraController
 */
export class CameraController {

    public canvas: HTMLCanvasElement
    public camera: Camera

    public initX: number
    public initY: number

    public prevX: number
    public prevY: number

    public rotateRate: number
    public panRate: number
    public zoomRate: number

    public offsetX: number
    public offsetY: number

    public onUpHandler = (e: MouseEvent) => { }
    public onMoveHandler = (e: MouseEvent) => { }

    constructor(gl: WebGL2RenderingContext, camera: Camera) {

        let box: ClientRect = gl.canvas.getBoundingClientRect()

        // Access our canvas to get our events
        this.canvas = gl.canvas
        // Reference to our camera and its transform class
        this.camera = camera

        this.offsetX = box.left
        this.offsetY = box.top

        // How fast to rotate
        this.rotateRate = -300
        // How fast to move
        this.panRate = 30
        // How fast to zoom
        this.zoomRate = 300

        // Starting and Previous X,Y position on mouse move
        this.prevX = 0
        this.prevY = 0
        this.initX = 0
        this.initY = 0

        this.onUpHandler = (e) => { this.onMouseUp(e) }
        this.onMoveHandler = (e) => { this.onMouseMove(e) }

        this.canvas.addEventListener("mousedown", (e: MouseEvent) => {
            this.onMouseDown(e)
        })
        this.canvas.addEventListener("mousewheel", (e: WheelEvent) => {
            this.onMouseWheel(e)
        })
        document.addEventListener("keydown", (e: KeyboardEvent) => {
            this.onKeyPress(e)
        })
    }

    public onKeyPress(e: KeyboardEvent): void {
        switch (e.keyCode) {
            // W
            case 87: {
                this.camera.moveZ(-STEP)
                break
            }
            // S
            case 83: {
                this.camera.moveZ(STEP)
                break
            }
            // A
            case 65: {
                this.camera.moveX(-STEP)
                break
            }
            // D
            case 68: {
                this.camera.moveX(STEP)
                break
            }
            default: break
        }
    }

    // Transform mouse x,y cords to something usable by the canvas
    public getMousePosition(e: MouseEvent): { x: number, y: number } {
        return {
            x: e.pageX - this.offsetX,
            y: e.pageY - this.offsetY
        }
    }

    // Dragging movement listener
    public onMouseDown(e: MouseEvent): void {
        this.initX = this.prevX = e.pageX - this.offsetX
        this.initY = this.prevY = e.pageY - this.offsetY

        this.canvas.addEventListener("mouseup", this.onUpHandler)
        this.canvas.addEventListener("mousemove", this.onMoveHandler)
    }

    // Remove draggin listener
    public onMouseUp(e: MouseEvent): void {
        this.canvas.removeEventListener("mouseup", this.onUpHandler)
        this.canvas.removeEventListener("mousemove", this.onMoveHandler)
    }

    // Zoom in/out
    public onMouseWheel(e: WheelEvent): void {
        // Normalize wheel number to -1 to 1, this is for the best according to stackoverflow
        let delta = Math.max(-1, Math.min(1, (e.wheelDelta || -e.detail)))
        this.camera.moveZ(delta * (this.zoomRate / this.canvas.height))
    }

    public onMouseMove(e: MouseEvent): void {
        // Get x,y position
        let x: number = e.pageX - this.offsetX,
            y: number = e.pageY - this.offsetY,
            // position deltas
            dx: number = x - this.prevX,
            dy: number = y - this.prevY

        // When shift is helt we move up/down on Y else we move we rotate on X
        e.shiftKey === true ?  
        this.camera.moveY(dy * (this.panRate / this.canvas.height)) :  
        this.camera.transform.rotation[1] += dx * (this.rotateRate / this.canvas.width)
  
        this.prevX = x
        this.prevY = y
    }
}