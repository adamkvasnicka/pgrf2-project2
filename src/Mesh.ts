import { ATTR_POSITION_LOC, ATTR_UV_LOC, ATTR_NORMAL_LOC } from "./locations/attrLocations"
import { Model } from "./Model"
import { VAO } from "./interfaces/vao"

/**
 * Class for building mesh for our 3D models
 * 
 * @export
 * @class Mesh
 */
export class Mesh {

    private meshCache: Array<VAO>
    public gl: WebGL2RenderingContext

    constructor(gl: WebGL2RenderingContext) {
        this.meshCache = []
        this.gl = gl
    }

    public createCube(name: string = "Cube", width: number = 1, height: number = 1, depth: number = 1,
        x: number = 0, y: number = 0, z: number = 0): Model {

        let w = width * 0.5,
            h = height * 0.5,
            d = depth * 0.5

        let x0 = x - w,
            x1 = x + w,
            y0 = y - h,
            y1 = y + h,
            z0 = z - d,
            z1 = z + d

        let totalSize = 16.0

        let vertices = []
        let step = 2.0 / totalSize
        let half = 2.0 / 2

        let indices: Array<number> = []

        for (let i = 0; i < 2.0; i += step) {
            for (let j = 0; j < 2; j++) {
                vertices.push(-half + (step) + i)       //x
                vertices.push(j)                        //y
                vertices.push(0)                         //z

                vertices.push(-half + (step) + step + i)        //x
                vertices.push(j)                                //y
                vertices.push(0)                                //z

                vertices.push(-half + (step) + step + i)            //x
                vertices.push(j)                                    //y
                vertices.push(-step)                                //z

                vertices.push(-half + (step) + i)           //x
                vertices.push(j)                            //y
                vertices.push(-step)                        //z
            }
        }

        // Build indices for each quad [0,1,2 2,3,0]
        for (let i = 0; i < vertices.length / 3; i += 8) {
            indices.push(i, i + 1, i + 2)
            indices.push(i, i + 2, i + 3)
            indices.push(i + 4, i + 5, i + 6)
            indices.push(i + 4, i + 6, i + 7)
            indices.push(i, i + 1, i + 5)
            indices.push(i, i + 4, i + 5)
            indices.push(i + 3, i + 4, i)
            indices.push(i + 3, i + 4, i + 7)
            indices.push(i + 3, i + 2, i + 6)
            indices.push(i + 7, i + 6, i + 3)
            indices.push(i + 1, i + 6, i + 2)
            indices.push(i + 5, i + 1, i + 6)
        }

        // Here we put all our vertices into vertex position buffer
        let mesh = this.createMeshVAO(this.gl.TRIANGLES, name, indices, vertices)
        mesh.noCulling = true // To view animation better

        this.meshCache[name] = mesh

        return new Model(mesh)
    }

    public createPoints(numberOfPoints: number = 16.0, sizeOfWhole: number = 1.8): Model {
        const gl = this.gl

        let vertices: Array<number> = [],
            step = sizeOfWhole / numberOfPoints,
            half = sizeOfWhole / 2

        for (let i = 0; i < numberOfPoints; i++) {
            for (let j = 0; j < numberOfPoints; j++) {
                vertices.push(-half + (j * step))  //x1
                vertices.push(1)                   //y1
                vertices.push(half - (i * step))   //z1
            }
        }

        const mesh = this.createMeshVAO(this.gl.POINTS, "PointGrid", null, vertices)

        this.meshCache["PointGrid"] = mesh

        return new Model(mesh)
    }

    public createGrid(numberOfPoints: number = 32.0, sizeOfWhole: number = 1.8): Model {

        // Size - W/H of the outer box of the grid
        // Div - How to divide up the grid

        const gl: WebGL2RenderingContext = this.gl

        // Dynamically create a grid
        let vertices: Array<number> = [],
            step = sizeOfWhole / numberOfPoints,     // Steps between each line, just a number we increment by for each line in the grid
            half = sizeOfWhole / 2                  // From origin the startting position is half the size

        let indices: Array<number> = []

        for (let i = 0; i < numberOfPoints; i++) {
            for (let j = 0; j < numberOfPoints; j++) {

                vertices.push(-half + (j * step))  //x1
                vertices.push(1)                   //y1
                vertices.push(half - (i * step))   //z1

                if ((j + 1) / numberOfPoints !== 1.0) {
                    indices.push((numberOfPoints * i) + j)
                    indices.push((numberOfPoints * i) + j + 1)
                }
                if (i > 0) {
                    indices.push((numberOfPoints * i) + j)
                    indices.push((numberOfPoints * i) + j - numberOfPoints)
                    // Triangulace
                    if (j > 0) {
                        indices.push((numberOfPoints * i) + j)
                        indices.push((numberOfPoints * i) + j - 1 - numberOfPoints)
                    }
                }
            }
        }

        const mesh = this.createMeshVAO(this.gl.LINES, "grid", indices, vertices)

        this.meshCache["Grid"] = mesh

        return new Model(mesh)
    }

    public createCircle(size: number = 1.8, div: number = 16.0): Model {
        // Size - W/H of the outer box of the grid
        // Div - How to divide up the grid

        const gl = this.gl

        // Dynamically create a grid
        let vertices: Array<any> = [],
            step = size / div,          // Steps between each line, just a number we increment by for each line in the grid
            half = size / 2             // From origin the startting position is half the size

        let r = 2

        let indices = []

        for (let j = 0; j < 8; j++) {
            let count = vertices.length / 3

            for (let i = 0; i < Math.PI / 180 * 360; i += Math.PI / 180 * (360 / 32)) {
                vertices.push(Math.sin(i) * (j + 1)) //x1
                vertices.push(0)  //y1
                vertices.push(Math.cos(i) * (j + 1))//z1
            }

            for (let i = count; i < (vertices.length / 3) - 1; i++) {
                indices.push(i)
                indices.push(i + 1)
            }
            indices.push(count)
            indices.push((vertices.length / 3) - 1)

        }

        const mesh: VAO = this.createMeshVAO(this.gl.LINES, "circle", indices, vertices)

        this.meshCache["CircleGrid"] = mesh

        return new Model(mesh)
    }

    // Setup vertex Shader Attributes using arrays
    private createMeshVAO(drawMode: number, name: string, arrayIndices: Array<number> | null, arrayVertices: Array<number>, arrayNormal?: Array<number> | null, arrayUV?: Array<number>, vertexLength?: number): VAO {
        let gl: WebGL2RenderingContext = this.gl

        let vao: VAO = { drawMode: drawMode }

        // Create and bind VAO
        vao.vao = gl.createVertexArray()
        gl.bindVertexArray(vao.vao) // Bind it so all the calls to vertexAttribPointer/enableVertexAttribArray is saved to vao

        //---------------
        // Set up vertices
        if (arrayVertices !== undefined && arrayVertices != null) {
            vao.bufVertices = gl.createBuffer()                                     // Create buffer...
            vao.vertexComponentLen = vertexLength || 3                              // How many floats (in X,Y,Z) make up a vertex
            vao.vertexCount = arrayVertices.length / vao.vertexComponentLen         // How many vertices in array

            gl.bindBuffer(gl.ARRAY_BUFFER, vao.bufVertices)
            gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(arrayVertices), gl.STATIC_DRAW)             // Then push array into it
            gl.enableVertexAttribArray(ATTR_POSITION_LOC)                                               // Enable Vertex Attribute using its location
            gl.vertexAttribPointer(ATTR_POSITION_LOC, vao.vertexComponentLen, gl.FLOAT, false, 0, 0)    // Tell shader what is what in attrib

            // Params of attrib pointer
            //
            // Attribute Location
            // How big is the vector by number count
            // What type of number we passing in
            // Does it need to be normalized
            // How big is a vertex chunk of data
            // Offset by how much
        }


        //---------------
        // Set up Normals
        if (arrayNormal !== undefined && arrayNormal != null) {
            vao.bufNormals = gl.createBuffer()

            gl.bindBuffer(gl.ARRAY_BUFFER, vao.bufNormals)
            gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(arrayNormal), gl.STATIC_DRAW)
            gl.enableVertexAttribArray(ATTR_NORMAL_LOC)
            gl.vertexAttribPointer(ATTR_NORMAL_LOC, 3, gl.FLOAT, false, 0, 0)
        }

        //--------------
        // Set up UV
        if (arrayUV !== undefined && arrayUV != null) {
            vao.bufUVs = gl.createBuffer()

            gl.bindBuffer(gl.ARRAY_BUFFER, vao.bufUVs)
            gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(arrayUV), gl.STATIC_DRAW)
            gl.enableVertexAttribArray(ATTR_UV_LOC)
            gl.vertexAttribPointer(ATTR_UV_LOC, 2, gl.FLOAT, false, 0, 0)
        }

        //--------------
        // Set up Indices
        if (arrayIndices !== undefined && arrayIndices != null) {
            vao.bufIndices = gl.createBuffer()
            vao.indexCount = arrayIndices.length

            gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, vao.bufIndices)
            gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(arrayIndices), gl.STATIC_DRAW) // Array of integers
        }

        // Its important to clean up VertexArray and other buffers
        gl.bindVertexArray(null)
        gl.bindBuffer(gl.ARRAY_BUFFER, null)
        if (arrayIndices != null && arrayIndices !== undefined) gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, null)

        return vao
    }

    public createSphere(latitude: number = 30, longitude: number = 30, radius: number = 2): Model {

        let vertices = []
        let indices = []
        let normals = []

        for (let i = 0; i <= latitude; i++) {

            let theta = i * Math.PI / latitude
            let sinTheta = Math.sin(theta)
            let cosTheta = Math.cos(theta)

            for (let j = 0; j < longitude; j++) {
                let phi = j * 2 * Math.PI / longitude
                let sinPhi = Math.sin(phi)
                let cosPhi = Math.cos(phi)

                let x = cosPhi * sinTheta
                let y = cosTheta
                let z = sinPhi * sinTheta

                normals.push(x)
                normals.push(y)
                normals.push(z)

                vertices.push(radius * x)
                vertices.push(radius * y)
                vertices.push(radius * z)
            }

        }

        for (let i = 0; i <= latitude; i++) {
            for (let j = 0; j < longitude; j++) {
                let first = (i * (longitude)) + j
                let second = first + longitude + 1
                if (first + 1 > (i * longitude - 1) + longitude) {
                    indices.push(first)
                    indices.push((i * longitude))
                } else {
                    indices.push(first)
                    indices.push(first + 1)
                }
                if (i > 0) {
                    indices.push(first)
                    indices.push(first - longitude)
                    indices.push(first)
                    indices.push(first + 1 - longitude)
                }
            }
        }

        let mesh = this.createMeshVAO(this.gl.LINES, "sphere", indices, vertices, normals)
        return new Model(mesh)
    }

    public createSkyBox(name: string,
        width: number = 50,
        height: number = 50,
        depth: number = 1,
        x: number = 0,
        y: number = 0,
        z: number = 0): Model {

        let w = width * 0.5,
            h = height * 0.5,
            d = depth * 0.5

        let x0 = x - w,
            x1 = x + w,
            y0 = y - h,
            y1 = y + h,
            z0 = z - d,
            z1 = z + d

        //Starting bottom left corner, then working counter clockwise to create the front face.
        let vertices = [
            x0, y1, z1, 0,	//0 Front
            x0, y0, z1, 0,	//1
            x1, y0, z1, 0,	//2
            x1, y1, z1, 0,	//3 

            x1, y1, z0, 1,	//4 Back
            x1, y0, z0, 1,	//5
            x0, y0, z0, 1,	//6
            x0, y1, z0, 1,	//7 

            x0, y1, z0, 2,	//7 Left
            x0, y0, z0, 2,	//6
            x0, y0, z1, 2,	//1
            x0, y1, z1, 2,	//0

            x0, y0, z1, 3,	//1 Bottom
            x0, y0, z0, 3,	//6
            x1, y0, z0, 3,	//5
            x1, y0, z1, 3,	//2

            x1, y1, z1, 4,	//3 Right
            x1, y0, z1, 4,	//2 
            x1, y0, z0, 4,	//5
            x1, y1, z0, 4,	//4

            x0, y1, z0, 5,	//7 Top
            x0, y1, z1, 5,	//0
            x1, y1, z1, 5,	//3
            x1, y1, z0, 5	//4
        ]

        //Build the index of each quad [0,1,2, 2,3,0]
        let indices = []
        for (let i = 0; i < vertices.length / 4; i += 2)
            indices.push(i, i + 1, (Math.floor(i / 4) * 4) + ((i + 2) % 4))

        //Build UV data for each vertex
        let textures = []
        for (let i = 0; i < 6; i++)
            textures.push(0, 0, 0, 1, 1, 1, 1, 0)

        //Build Normal data for each vertex
        let normals = [
            0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1,		    //Front
            0, 0, -1, 0, 0, -1, 0, 0, -1, 0, 0, -1,		//Back
            -1, 0, 0, -1, 0, 0, -1, 0, 0, -1, 0, 0,		//Left
            0, -1, 0, 0, -1, 0, 0, -1, 0, 0, -1, 0,		//Bottom
            1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0,		    //Right
            0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0		    //Top
        ]

        let mesh = this.createMeshVAO(this.gl.TRIANGLES, name, indices, vertices, normals, textures, 4)
        this.meshCache[name] = mesh

        mesh.noCulling = true;	// Only setting this true to view animations better.
        return new Model(mesh);
    }

    //-------------------------------------
    // GETTERS
    //-------------------------------------
    public getMesh(name: string): VAO {
        return this.meshCache[name]
    }

}