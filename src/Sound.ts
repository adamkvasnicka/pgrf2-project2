/**
 * Sound representation using HTML5 web audio API
 * https://www.html5rocks.com/en/tutorials/webaudio/intro/#toc-filter
 * https://developer.mozilla.org/en-US/docs/Web/API/Web_Audio_API
 * 
 * @export
 * @class Sound
 */
export class Sound {

    public audioContext: AudioContext
    public audioAnalyser: AnalyserNode
    public frequencyData: any
    public audio: HTMLAudioElement

    public audioSource: MediaElementAudioSourceNode
    public gainNode: GainNode
    public lowPassFilter: BiquadFilterNode
    public highPassFilter: BiquadFilterNode
    public bassFilter: BiquadFilterNode
    public trembleFilter: BiquadFilterNode
    public midFilter: BiquadFilterNode

    public playbackRate: AudioBufferSourceNode

    constructor(fftSize: number = 4096) {
        // HTML5 Web audio context
        this.audioContext = new AudioContext()
        this.audio = <HTMLAudioElement>document.getElementById("myAudio")

        this.audio.playbackRate = 1.0

        // Source node, first node in the audio chain
        this.audioSource = this.audioContext.createMediaElementSource(this.audio as HTMLAudioElement)
        // Analyser node for representating real-time frequency and time-domain analysis information
        this.audioAnalyser = this.audioContext.createAnalyser()
        // Size of Fast Fourier Transform, its used to determinate the frequency domain
        this.audioAnalyser.fftSize = fftSize
        // Minimum value for the range of results
        this.audioAnalyser.minDecibels = -90
        // Maximum value for the range of results
        this.audioAnalyser.maxDecibels = -10
        // Making transition between values over time smoother
        this.audioAnalyser.smoothingTimeConstant = 0.85

        // Making playbackRate node
        this.playbackRate = this.audioContext.createBufferSource()
        this.playbackRate.playbackRate.setValueAtTime(1.0, 0)

        // Gain node for volume controll
        this.gainNode = this.audioContext.createGain()


        //--------------------------------------------------------
        // Biquad filters
        // Biqaud filter node represent a simple low-order filter
        //--------------------------------------------------------
        // Frequencies lower than the frequency gets a boost
        this.bassFilter = this.audioContext.createBiquadFilter()
        this.bassFilter.type = "lowshelf"
        // select which frequencies to boost, so it will boost everything below 200
        this.bassFilter.frequency.setValueAtTime(200, 0)

        // Frequencies higher than the frequncy gets a boost
        this.trembleFilter = this.audioContext.createBiquadFilter()
        this.trembleFilter.type = "highshelf"
        // select which frequencies to boost, so it will boost everything above 2000
        this.trembleFilter.frequency.setValueAtTime(2000, 0)

        // Frequencies inside the range gets a boost, outsides remain unchanged
        this.midFilter = this.audioContext.createBiquadFilter()
        this.midFilter.type = "peaking"

        // Passing filters
        this.lowPassFilter = this.audioContext.createBiquadFilter()
        // Standart second-order resonant lowpass filter
        this.lowPassFilter.type = "lowpass"
        // Represent a frequency in the current filtering algorithm ["lowpass","highpass",...], its measured in Hz
        this.lowPassFilter.frequency.setValueAtTime(440, 0)

        this.highPassFilter = this.audioContext.createBiquadFilter()
        this.highPassFilter.type = "highpass"
        this.highPassFilter.frequency.setValueAtTime(0, 0)


        //----------------------
        // CONNECT AUDIO NODES
        //----------------------
        this.connect()

        // Initialize our frequency data, we will use this array to store current values
        this.frequencyData = new Uint8Array(this.audioAnalyser.frequencyBinCount)
    }

    public connect(): Sound {
        // Here we are connecting nodes in chain kind of style
        // Be carefull of order
        this.audioSource.connect(this.lowPassFilter)

        this.lowPassFilter.connect(this.highPassFilter)
        this.highPassFilter.connect(this.bassFilter)
        this.bassFilter.connect(this.trembleFilter)
        this.trembleFilter.connect(this.midFilter)
        this.midFilter.connect(this.audioAnalyser)

        this.audioAnalyser.connect(this.gainNode)
        this.gainNode.connect(this.audioContext.destination)

        return this
    }

    public disconnectAll(number: number = 0): Sound {
        this.audioSource.disconnect(number)
        this.lowPassFilter.disconnect(number)
        this.bassFilter.disconnect(number)
        this.trembleFilter.disconnect(number)
        this.midFilter.disconnect(number)
        this.audioAnalyser.disconnect(number)
        this.gainNode.disconnect(number)

        return this
    }

    public getSoundSample(): Float32Array {
        this.audioAnalyser.getByteFrequencyData(this.frequencyData)
        return new Float32Array(this.normalize(new Float32Array(this.frequencyData)))
    }

    public changeSource(src): Sound {
        this.audio.pause()
        this.audio.src = src
        this.disconnectAll().connect()
        this.audio.play()
        return this
    }

    private normalize(list): Float32Array {
        const minMax = list.reduce((acc, value) => {
            if (value < acc.min) {
                acc.min = value;
            }
            if (value > acc.max) {
                acc.max = value;
            }
            return acc;
        }, { min: 0, max: 1 });
        return list.map(value => {
            const diff = minMax.max - minMax.min;
            return (value - minMax.min) / diff;
        });
    }

}
