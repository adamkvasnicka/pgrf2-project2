import { ATTR_POSITION_LOC, ATTR_UV_LOC, ATTR_NORMAL_LOC } from "./locations/attrLocations"

/**
 *  webGL configuration class
 * 
 * @export
 * @class GL
 */
export class GL {

    public gl: WebGL2RenderingContext
    public canvas: HTMLCanvasElement | any

    constructor(canvasID: string) {
        this.canvas = document.getElementById(canvasID)
        this.gl = this.canvas.getContext("webgl2")

        // Check if webGL is available
        if (!this.gl) console.error("WebGL is not available")

        // Setup GL, Set all the default configurations we need

        // Cutting, cut back facing things (Back is also default)
        this.gl.cullFace(this.gl.BACK)
        // Its also default value
        this.gl.frontFace(this.gl.CCW)
        // Enable ZBuffer           
        this.gl.enable(this.gl.DEPTH_TEST)
        // Only front facing triangles
        this.gl.enable(this.gl.CULL_FACE)
        // Near things obscure far things 
        this.gl.depthFunc(this.gl.LEQUAL)
        // Setup default alpha blending, Dont know what this does that much
        this.gl.blendFunc(this.gl.SRC_ALPHA, this.gl.ONE_MINUS_SRC_ALPHA)

        // Clear color
        this.gl.clearColor(0.0, 0.0, 0.0, 1.0)
    }

    // Clear canvas and Color and Zbuffers
    public clear(): GL {
        // It will use clear color above
        this.gl.clear(this.gl.COLOR_BUFFER_BIT | this.gl.DEPTH_BUFFER_BIT)
        return this
    }

    // Viewport size setup
    public setSize(width: number, height: number): GL {
        this.canvas.style.width = `${width}px`
        this.canvas.style.height = `${height}px`
        this.canvas.width = width
        this.canvas.height = height

        // When updating canvas size, must reset the viewport of the canvas
        // Else the resolution webgl renders at will not change
        this.gl.viewport(0, 0, width, height)

        return this
    }

    // Setup the size of the canvas to fill a % of the total screen
    public fitScreen(widthPercentage: number = 1, heightPercentage: number = 1): GL {
        // Setting the size and returning GL for possible chaining
        return this.setSize(window.innerWidth * (widthPercentage), window.innerHeight * (heightPercentage))
    }

    // GL context Getter
    public getGL(): WebGL2RenderingContext {
        return this.gl
    }

}