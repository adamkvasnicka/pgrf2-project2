import { ATTR_NORMAL_LOC, ATTR_NORMAL_NAME, ATTR_UV_NAME, ATTR_POSITION_LOC, ATTR_POSITION_NAME, ATTR_UV_LOC } from "../locations/attrLocations"
import { AttribLocation, UniformLocation } from "../interfaces/locations";

/**
 * Utility class for Shaders
 * 
 * @export
 * @class ShaderUtil
 */
export class ShaderUtil {

    // Compile and create GL shader
    static createShader(gl: WebGL2RenderingContext, src: string, type: number): WebGLShader {
        const shader: WebGLShader = gl.createShader(type)
        gl.shaderSource(shader, src)
        gl.compileShader(shader)

        // Get Error data if shader failed compiling
        if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
            console.error(`Error has ocured when compiling shader: ${src}`, gl.getShaderInfoLog(shader))
            gl.deleteShader(shader)
            return null
        }

        return shader
    }

    // Creating GPU program
    static createProgram(gl: WebGL2RenderingContext, verShader: WebGLShader, fragShader: WebGLShader): WebGLProgram {
        const program: WebGLProgram = gl.createProgram()
        gl.attachShader(program, verShader)
        gl.attachShader(program, fragShader)

        // Force predefined locations for specific attributes. If the attribute isnt used in the shader its location will default to -1
        gl.bindAttribLocation(program, ATTR_POSITION_LOC, ATTR_NORMAL_NAME)
        gl.bindAttribLocation(program, ATTR_NORMAL_LOC, ATTR_NORMAL_NAME)
        gl.bindAttribLocation(program, ATTR_UV_LOC, ATTR_UV_NAME)

        // And link both shaders together
        gl.linkProgram(program)

        // Check for errors
        if (!gl.getProgramParameter(program, gl.LINK_STATUS)) {
            console.error("Error creating shader program", gl.getProgramInfoLog(program))
            gl.deleteProgram(program)
            return null
        }

        // Additional error check for debugging
        gl.validateProgram(program)
        if (!gl.getProgramParameter(program, gl.VALIDATE_STATUS)) {
            console.error("Error validating program", gl.getProgramInfoLog(program))
            gl.deleteProgram(program)
            return null
        }

        // Can detach and delete shaders since program was made
        gl.detachShader(program, verShader)
        gl.detachShader(program, fragShader)
        gl.deleteShader(verShader)
        gl.deleteShader(fragShader)

        return program
    }

    // Compile shaders and create a program
    static compileProgram(gl: WebGL2RenderingContext, vertexShaderText: string, fragmentShaderText: string): WebGLProgram | null {
        // Compiling shaders
        let vertexShader = ShaderUtil.createShader(gl, vertexShaderText, gl.VERTEX_SHADER)
        if (!vertexShader) return null
        let fragmentShader = ShaderUtil.createShader(gl, fragmentShaderText, gl.FRAGMENT_SHADER)
        // If creating fragment shader fails, we delete vertex shader too
        if (!fragmentShader) {
            gl.deleteShader(vertexShader)
            return null
        }
        return ShaderUtil.createProgram(gl, vertexShader, fragmentShader)
    }

    //--------------------------------------------
    // Location getters
    //--------------------------------------------

    // Get the locations of main Attributes (Vertices, Normals and Textures)
    static getAttribLocation(gl: WebGL2RenderingContext, program: WebGLProgram): AttribLocation {
        return {
            position: gl.getAttribLocation(program, ATTR_POSITION_NAME),
            normal: gl.getAttribLocation(program, ATTR_NORMAL_NAME),
            uv: gl.getAttribLocation(program, ATTR_UV_NAME)
        }
    }

    // Get location of basic uniforms (Projection, Model, Camera and Used texture)
    static getUniformLocation(gl: WebGL2RenderingContext, program: WebGLProgram): UniformLocation {
        return {
            perspectiveMatrix: gl.getUniformLocation(program, "uPMatrix"),
            modelMatrix: gl.getUniformLocation(program, "uMVMatrix"),
            cameraMatrix: gl.getUniformLocation(program, "uCameraMatrix"),
            mainTexture: gl.getUniformLocation(program, "uMainTex")
        }
    }

}