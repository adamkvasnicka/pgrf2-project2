import { Shader } from "./Shader"
import { POINT_FRAGMENT, POINT_VERTEX } from "../GLSL/point/shader"

/**
 * 
 * @export
 * @class GridShader
 * @extends {Shader}
 */
export class PointShader extends Shader {

    constructor(gl: WebGL2RenderingContext, pMatrix: any) {
        super(gl, POINT_VERTEX, POINT_FRAGMENT)

        // Custom uniforms
        this.uniformLocation.frequency = gl.getUniformLocation(this.program, "uSoundFreq")

        // Standart uniforms, we have to always activate our program
        this.setPerspectiveMatrix(pMatrix)

        // finish setting up shader
        gl.useProgram(null)
    }

    public setFrequency(frequency: Float32Array): PointShader {
        this.gl.uniform1fv(this.uniformLocation.frequency, frequency)
        return this
    }

}