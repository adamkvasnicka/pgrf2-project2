import { Shader } from "./Shader"
import { GRID_FRAGMENT, GRID_VERTEX } from "../GLSL/grid/shader"

/**
 * 
 * @export
 * @class GridShader
 * @extends {Shader}
 */
export class GridShader extends Shader {

    constructor(gl: WebGL2RenderingContext, pMatrix: any) {

        super(gl, GRID_VERTEX, GRID_FRAGMENT)

        // Custom uniforms
        this.uniformLocation.frequency = gl.getUniformLocation(this.program, "uSoundFreq")

        // Standart uniforms, we have to always activate our program
        this.setPerspectiveMatrix(pMatrix)

        // Finish setting up shader
        gl.useProgram(null)
    }

    public setFrequency(frequency: Float32Array): GridShader {
        this.gl.uniform1fv(this.uniformLocation.frequency, frequency)
        return this
    }

}