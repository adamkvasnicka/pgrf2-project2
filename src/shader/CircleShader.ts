import { Shader } from "./Shader"
import { CIRCLE_FRAGMENT, CIRCLE_VERTEX } from "../GLSL/circle/shader"

/**
 * 
 * @export
 * @class CircleShader
 * @extends {Shader}
 */
export class CircleShader extends Shader {

    constructor(gl: WebGL2RenderingContext, pMatrix: any) {

        super(gl, CIRCLE_VERTEX, CIRCLE_FRAGMENT)

        // Custom uniforms
        this.uniformLocation.frequency = gl.getUniformLocation(this.program, "uSoundFreq")
        this.uniformLocation.radius = gl.getUniformLocation(this.program, "radius")

        // Standart uniforms, we have to always activate our program
        this.setPerspectiveMatrix(pMatrix)

        // Finish setting up shader
        gl.useProgram(null)
    }

    public setFrequency(frequency: Float32Array): CircleShader {
        const radius: number = frequency.reduce((acc, value) => acc += value, 0) / frequency.length
        const newFrequencyArray: Float32Array = frequency.map(val => val / 2)

        this.gl.uniform1f(this.uniformLocation.radius, radius)
        this.gl.uniform1fv(this.uniformLocation.frequency, new Float32Array(newFrequencyArray))
        return this
    }

}