import { Shader } from "./Shader"
import { CUBE_FRAGMENT, CUBE_VERTEX } from "../GLSL/cube/shader"

/**
 * 
 * @export
 * @class CubeShader
 * @extends {Shader}
 */
export class CubeShader extends Shader {

    public mainTexture: any

    constructor(gl: WebGL2RenderingContext, pMatrix: any) {

        super(gl, CUBE_VERTEX, CUBE_FRAGMENT)

        // Custom uniforms
        this.uniformLocation.frequency = gl.getUniformLocation(this.program, "uSoundFreq")
    
        this.setPerspectiveMatrix(pMatrix)

        // Finish setting up
        gl.useProgram(null)
    }

    public setFrequency(frequency: Float32Array): CubeShader {
        this.gl.uniform1fv(this.uniformLocation.frequency, frequency)
        return this
    }

}