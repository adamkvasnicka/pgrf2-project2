import { Shader } from "./Shader"
import { SPHERE_FRAGMENT, SPHERE_VERTEX } from "../GLSL/sphere/shader"

/**
 * 
 * @export
 * @class SphereShader
 * @extends {Shader}
 */
export class SphereShader extends Shader {

    public histoFrequency: Array<number>

    public time: number

    constructor(gl: WebGL2RenderingContext, pMatrix: any) {

        super(gl, SPHERE_VERTEX, SPHERE_FRAGMENT)

        this.histoFrequency = Array.apply(null, Array(60)).map(Number.prototype.valueOf, 1.5)
        this.time = 0

        // Custom uniforms
        this.uniformLocation.frequency = gl.getUniformLocation(this.program, "uSoundFreq")
   

        this.setPerspectiveMatrix(pMatrix)

        // Finish setting up shader
        gl.useProgram(null)
    }

    public setFrequencyAndTime(frequency: Float32Array, time: number): SphereShader {
        const timeNow: number = Math.round(time / 10)
        if (timeNow > this.time) {
            this.time = timeNow
            const currentFrequency: Array<number> = Array.prototype.slice.call(frequency)
            currentFrequency.forEach((value) => {
                this.histoFrequency.unshift(1.5 - value * 2)
                this.histoFrequency.pop()
            })
        }
        this.gl.uniform1fv(this.uniformLocation.frequency, new Float32Array(this.histoFrequency))
        return this
    }

}