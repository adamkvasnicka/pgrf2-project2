import { Shader } from "./Shader"
import { CONT_FRAGMENT, CONT_VERTEX } from "../GLSL/cont/shader"

/**
 * 
 * @export
 * @class ContShader
 * @extends {Shader}
 */
export class ContShader extends Shader {

    private time: number
    private frozenFrequency: Array<number>

    constructor(gl: WebGL2RenderingContext, pMatrix: any) {
        super(gl, CONT_VERTEX, CONT_FRAGMENT)

        this.time = 0
        this.frozenFrequency = Array.apply(null, Array(1024)).map(Number.prototype.valueOf, 0)

        // Custom uniforms
        this.uniformLocation.frequency = gl.getUniformLocation(this.program, "uSoundFreq")

        // Standart uniforms, we have to always activate our program
        this.setPerspectiveMatrix(pMatrix)

        // finish setting up shader
        gl.useProgram(null)
    }

    public setTime(t: number): ContShader {
        const timeNow: number = Math.round(t / 500)
        if (timeNow > this.time) {
            this.time = timeNow
            this.frozenFrequency.unshift(this.time)
            this.frozenFrequency.pop()
        }
        return this
    }

    public setFrequencyAndTime(frequency: Float32Array, time: number): ContShader {
        const timeNow: number = Math.round(time / 60)
        if (timeNow > this.time) {
            this.time = timeNow
            let average: number = Array.prototype.slice.call(frequency).reduce((acc, item) => acc += item, 0) / frequency.length * 2
            let sizeOfFreqDomain: number = this.frozenFrequency.length
            

            this.frozenFrequency.unshift(...Array.prototype.slice.call(frequency))
            this.frozenFrequency.splice(1024, 32)
        }

        this.gl.uniform1fv(this.uniformLocation.frequency, new Float32Array(this.frozenFrequency))
        return this
    }

}