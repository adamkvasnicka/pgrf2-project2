import { Shader } from "./Shader"
import { SKY_FRAGMENT, SKY_VERTEX } from "../GLSL/skybox/shader"

/**
 * 
 * @export
 * @class SkyMapShader
 * @extends {Shader}
 */
export class SkyMapShader extends Shader {

    public skyTex: any

    constructor(gl: WebGL2RenderingContext, pMatrix: any, skyTex: any) {

        super(gl, SKY_VERTEX, SKY_FRAGMENT)

        // In custom uniforms we are saving location of custom uniforms for future use
        // Custom uniforms
        this.uniformLocation.skyTex = gl.getUniformLocation(this.program, "uSkyTex")

        // Standart uniforms
        this.setPerspectiveMatrix(pMatrix)
        this.skyTex = skyTex

        // Finish setting up shader
        gl.useProgram(null)
    }

    // Setting up texture before rendering
    public setTexture(): SkyMapShader {
        // Setup Texture
        this.gl.activeTexture(this.gl.TEXTURE0)
        this.gl.bindTexture(this.gl.TEXTURE_CUBE_MAP, this.skyTex)
        this.gl.uniform1i(this.uniformLocation.skyTex, 0)
        return this
    }

}