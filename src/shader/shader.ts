import { ShaderUtil } from "./ShaderUtil"
import { UniformLocation, AttribLocation } from "../interfaces/locations";

/**
 * Basic shader 
 * 
 * @export
 * @class Shader
 */
export class Shader {

    public program: WebGLProgram | null
    public gl: WebGL2RenderingContext

    public attribLocation: AttribLocation | any
    public uniformLocation: UniformLocation | any

    constructor(gl: WebGL2RenderingContext, vertexShaderSource: string, fragmentShaderSource: string) {
        this.program = ShaderUtil.compileProgram(gl, vertexShaderSource, fragmentShaderSource)
        this.gl = gl
        this.attribLocation = {}
        this.uniformLocation = {}

        if (this.program != null) {
            gl.useProgram(this.program)
            this.attribLocation = ShaderUtil.getAttribLocation(gl, this.program)
            this.uniformLocation = ShaderUtil.getUniformLocation(gl, this.program)
        }

        // Note:: Extended shaders should deactivate shader when done calling super and setting up custom parts in constructor
    }

    // Program utilities
    public activate(): any {
        this.gl.useProgram(this.program)
        return this
    }
    public deactivate(): any {
        this.gl.useProgram(null)
        return this
    }

    setPerspectiveMatrix(matrix): any {
        this.gl.uniformMatrix4fv(this.uniformLocation.perspectiveMatrix, false, matrix)
        return this
    }
    setModelMatrix(matrix): any {
        this.gl.uniformMatrix4fv(this.uniformLocation.modelMatrix, false, matrix)
        return this
    }
    setCameraMatrix(matrix): any {
        this.gl.uniformMatrix4fv(this.uniformLocation.cameraMatrix, false, matrix)
        return this
    }

    // Setting up a texture
    public setTexture(): any {
    }

    // Setting frequency and runtime
    public setFrequencyAndTime(frequency: Float32Array, time: number): any {
    }

    // Setting audio frequency
    public setFrequency(frequency: Float32Array): any {
    }

    // Render model
    public renderModel(model: any): any {

        // This is where we send transformed matrix into our vertex shader
        this.setModelMatrix(model.transform.getViewMatrix()) // Set the transform, so the shader knows where to model exists in 3d space

        this.gl.bindVertexArray(model.mesh.vao) // Enable VAO, this will set all the predefined attributes for the shader

        if (model.mesh.noCulling) this.gl.disable(this.gl.CULL_FACE)

        if (model.mesh.indexCount) {
            this.gl.drawElements(model.mesh.drawMode, model.mesh.indexCount, this.gl.UNSIGNED_SHORT, 0)
        } else {
            this.gl.drawArrays(model.mesh.drawMode, 0, model.mesh.vertexCount)
        }

        this.gl.bindVertexArray(null)

        // Enable/disable culling
        if (model.mesh.noCulling) this.gl.enable(this.gl.CULL_FACE)

        return this
    }

}