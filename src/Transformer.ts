import { vec3, mat4, quat, mat2, mat3, vec4 } from "gl-matrix";

/**
 * Transformation class
 * @export
 * @class Transform
 */
export class Transform {

    public position: vec3
    public scale: vec3
    public rotation: vec3

    public matView: mat4
    public matNormal: mat3

    // Camera vectors
    public forward: vec4
    public up: vec4
    public right: vec4

    constructor() {

        // NOTE:: All vec3, vec4, mat3, mat4 etc. are just plain one dimensional arrays

        this.position = vec3.fromValues(0, 0, 0)    // vec3 - Position of object we are transforming
        this.scale = vec3.fromValues(1, 1, 1)       // vec3 - Scale vector, 1 means no scalling is done
        this.rotation = vec3.fromValues(0, 0, 0)    // vec3 - Rotation vector, using degrees

        this.matView = mat4.create()        // mat4 - Our transformation Mat4 Matrix, This will be Model or View matrix
        this.matNormal = mat3.create()      // mat3 Matrix, that will hold our normal values

        this.forward = vec4.create()        // vec4 - When using camera rotation this will tell us what the forward direction is
        this.up = vec4.create()             // vec4 - UP direction, invert to get bottom direction
        this.right = vec4.create()          // vec4 - Right direction, invert to get left direction

        this.reset() // Reset all vec3
    }

    // Setting position, scale, and rotation transformation using GL-Matrix library
    public setPosition(x: number, y: number, z: number): void {
        vec3.set(this.position, x, y, z)
    }

    public setRotation(x: number, y: number, z: number): void {
        vec3.set(this.rotation, x, y, z)
    }

    public setScale(x: number, y: number, z: number): void {
        vec3.set(this.scale, x, y, z)
    }

    // Incrementing scale by a number (0 is X, 1 is Y, 2 is Z)
    public addScale(x: number, y: number, z: number): void {
        this.scale[0] += x
        this.scale[1] += y
        this.scale[2] += z
    }

    // Increment position
    public addPosition(x: number, y: number, z: number): void {
        this.position[0] += x
        this.position[1] += y
        this.position[2] += z
    }

    // Increment rotation
    public addRotation(x: number, y: number, z: number): void {
        this.rotation[0] += x
        this.rotation[1] += y
        this.rotation[2] += z
    }

    // Updates direction of our camera
    public updateDirection(): Transform {
        // Will take our viewMatrix and multiply it with vec4 to get facing vectors
        // NOTE:: Z is Forwards, Y is UP, X is Right
        vec4.transformMat4(this.forward, [0, 0, 1, 0], this.matView) // Z
        vec4.transformMat4(this.up, [0, 1, 0, 0], this.matView)      // Y
        vec4.transformMat4(this.right, [1, 0, 0, 0], this.matView)   // X
        return this
    }

    // Update our viewMatrix with given vectors - positon, scale, rotation
    public updateMatrix(): mat4 {
        // NOTE:: We must reset our model matrix, simply set it as indentity matrice
        this.resetModelMatrix()
        // Using GL-Matrix library, according to docs this is the fastest way to mul our model matrix
        const rotationQuat: quat = quat.create()
        quat.fromEuler(rotationQuat, this.rotation[0], this.rotation[2], this.rotation[1])
        // Keep and eye on order
        mat4.fromRotationTranslationScale(this.matView, rotationQuat, this.position, this.scale)

        return this.matView
    }

    // Make our model matrix identity
    public resetModelMatrix(): mat4 {
        mat4.identity(this.matView)
        return this.matView
    }

    // Reset our transformation vectors
    public reset(): Transform {
        vec3.set(this.scale, 1, 1, 1)
        vec3.set(this.position, 0, 0, 0)
        vec3.set(this.rotation, 0, 0, 0)
        // Returning this for possible chaining
        return this
    }

    //-----------------------------
    // GETTERS
    //-----------------------------

    public getViewMatrix(): mat4 {
        return this.matView
    }

    public getNormalMatrix(): any {
        return this.getNormalMatrix
    }
}