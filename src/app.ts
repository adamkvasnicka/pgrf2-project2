import { Camera } from "./Camera"
import { GL } from "./gl"
import { RenderLoop } from "./utils/RenderLoop"
import { Model } from "./Model"
import { CameraController } from "./CameraController"
import { GridShader } from "./shader/GridShader"
import { SkyMapShader } from "./shader/SkyMapShader"
import { Mesh } from "./Mesh"
import { Resources } from "./utils/Resources"
import { SoundController } from "./SoundController"
import { Sound } from "./Sound"
import { CircleShader } from "./shader/CircleShader"
import { Utils } from "./utils/Utils"
import { Shader } from "./shader/Shader"
import { SphereShader } from "./shader/SphereShader"
import { PointShader } from "./shader/PointShader"
import { CubeShader } from "./shader/CubeShader"
import { CUBES, CIRCLE, GRID, SPHERE, POINTS, CONT } from "./utils/constants"
import { ContShader } from "./shader/ContShader";

/**
 * Application class
 * 
 * @class App
 */
export class App {

    public webGL: WebGL2RenderingContext
    public GL: GL
    public mesh: Mesh
    public resources: Resources

    public camera: Camera
    public cameraController: CameraController

    public shaderCache: Array<any>
    public modelCache: Array<any>

    public sound: Sound
    public soundController: SoundController

    public cubeSound: Sound
    public cubeSoundController: SoundController

    public loop: RenderLoop

    public sizeOfGrid: number
    public gridController: HTMLInputElement

    constructor() {

        this.GL = new GL("glCanvas").fitScreen().clear()
        this.webGL = this.GL.getGL()
        this.mesh = new Mesh(this.webGL)
        this.resources = new Resources(this.webGL)

        this.camera = new Camera(this.webGL)
        this.cameraController = new CameraController(this.webGL, this.camera)

        this.loop = new RenderLoop()

        this.modelCache = []
        this.shaderCache = []

        // GRID controller
        this.gridController = document.getElementById("gridSize") as HTMLInputElement
        this.sizeOfGrid = parseInt(this.gridController.value)
        this.gridController.addEventListener("input", () => {
            this.controllSizeOfGrid(parseInt(this.gridController.value))
        })

        this.initCamera()
        this.loadResources()
        this.loadshaders()
        this.loadModels()

        this.sound = new Sound()
        this.soundController = new SoundController(this.sound)
    }

    public start(onRender: any, fps: number = 60): void {
        // When stop only audio will stop, loop will still run
        this.controllSizeOfGrid(parseInt(this.gridController.value))
        this.sound.audio.play()
        this.loop.setCallback(onRender).setFPS(fps).start()
    }

    public stop(): void {
        this.sound.audio.pause()
        this.loop.stop()
    }

    public initCamera(): void {
        // Setting our initial position
        this.camera.transform.reset()
        this.camera.transform.setPosition(0, 2, 3)
    }

    public loadResources(): void {
        // Load skymap texture
        this.resources.loadCubeMap("skybox", [
            document.getElementById("cube_right"),
            document.getElementById("cube_left"),
            document.getElementById("cube_top"),
            document.getElementById("cube_bottom"),
            document.getElementById("cube_back"),
            document.getElementById("cube_front")
        ])
    }

    public loadshaders(): void {

        this.shaderCache["GridShader"] = new GridShader(this.webGL, this.camera.projectionMatrix)

        this.shaderCache["PointShader"] = new PointShader(this.webGL, this.camera.projectionMatrix)

        this.shaderCache["ContShader"] = new ContShader(this.webGL, this.camera.projectionMatrix)

        this.shaderCache["CircleShader"] = new CircleShader(this.webGL, this.camera.projectionMatrix)

        this.shaderCache["CubeShader"] = new CubeShader(this.webGL, this.camera.projectionMatrix)

        this.shaderCache["SphereShader"] = new SphereShader(this.webGL, this.camera.projectionMatrix)

        this.shaderCache["SkyMapShader"] = new SkyMapShader(this.webGL, this.camera.projectionMatrix, this.resources.getTexture("skybox"))

    }

    public loadModels(): void {

        this.modelCache["GridModel"] = this.mesh.createGrid()

        this.modelCache["HistoGridModel"] = this.mesh.createGrid()

        this.modelCache["CubeModel"] = this.mesh.createCube()

        this.modelCache["CubeModel"].setPosition(0, 1, 0).setScale(1, 2, 1)

        this.modelCache["CircleGrid"] = this.mesh.createCircle()

        this.modelCache["PointModel"] = this.mesh.createPoints()

        this.modelCache["SphereModel"] = this.mesh.createSphere()

        this.modelCache["SkyboxModel"] = this.mesh.createSkyBox("Skybox", 50, 50, 50, 0, 0, 0)

    }

    public getSoundSample(sizeOfArray: number = 256): Float32Array {
        return Utils.convertFFT(this.sound.getSoundSample(), sizeOfArray)
    }

    public controllSizeOfGrid(value: number): void {
        this.sizeOfGrid = value
        this.modelCache["GridModel"] = this.mesh.createGrid(Math.floor(Math.sqrt(this.sizeOfGrid)))
    }

    public getSizeOfGrid(): number {
        return this.sizeOfGrid
    }
}

// Application initialization
window.addEventListener("load", () => {

    //----------------------------------
    // Application class initialization
    const app: App = new App()
    //----------------------------------

    const typeButton: HTMLElement = document.querySelector("#animation-toggle")
    const filterToggle: HTMLElement = document.querySelector("#filter-toggle")
    const controllToggle: HTMLElement = document.querySelector("#controll-toggle")
    const playlistToggle: HTMLElement = document.querySelector("#playlist-toggle")

    typeButton.addEventListener("click", () => {
        document.querySelector(".animation-container").classList.toggle("element-hidden")
    })

    filterToggle.addEventListener("click", () => {
        document.querySelector(".filter-container").classList.toggle("filter-hidden")
    })

    controllToggle.addEventListener("click", () => {
        document.querySelector(".controll-container").classList.toggle("filter-hidden")
    })

    playlistToggle.addEventListener("click", () => {
        document.querySelector(".playlist-container").classList.toggle("filter-hidden")
    })

    const playlist: any = document.querySelectorAll(".playlist-container a")
    for (let i = 0; i < playlist.length; i++) {
        playlist[i].addEventListener("click", (e) => {
            e.preventDefault()
            app.sound.changeSource(playlist[i].getAttribute("data-src"))
        })
    }

    const skyboxCheck: HTMLInputElement = <HTMLInputElement>document.getElementById("skybox-check")

    const grid: HTMLButtonElement = <HTMLButtonElement>document.getElementById("gridButton")
    const cube: HTMLButtonElement = <HTMLButtonElement>document.getElementById("cubeButton")
    const point: HTMLButtonElement = <HTMLButtonElement>document.getElementById("pointButton")
    const circle: HTMLButtonElement = <HTMLButtonElement>document.getElementById("circleButton")
    const sphere: HTMLButtonElement = <HTMLButtonElement>document.getElementById("sphereButton")
    const continous: HTMLButtonElement = <HTMLButtonElement>document.getElementById("continuousButton")

    let selected: string = GRID

    const gridSize: HTMLElement = <HTMLElement>document.getElementById("size-controller")

    grid.addEventListener("click", () => {
        gridSize.classList.remove("hide-element")
        selected = GRID
        app.initCamera()
    })
    continous.addEventListener("click", () => {
        gridSize.classList.add("hide-element")
        selected = CONT
        app.initCamera()
    })
    point.addEventListener("click", () => {
        gridSize.classList.add("hide-element")
        selected = POINTS
        app.initCamera()
    })
    cube.addEventListener("click", () => {
        gridSize.classList.add("hide-element")
        selected = CUBES
        app.initCamera()
    })
    circle.addEventListener("click", () => {
        gridSize.classList.add("hide-element")
        selected = CIRCLE
        app.initCamera()
        app.camera.transform.setPosition(0, 1, 7)
    })
    sphere.addEventListener("click", () => {
        gridSize.classList.add("hide-element")
        selected = SPHERE
        app.initCamera()
        app.camera.transform.setPosition(0, 1, 12)
    })

    // We will switch shader and so on here because we can run only one shader at time
    const onRender = (deltaTime: number) => {

        app.camera.updateViewMatrix()
        app.GL.clear()

        switch (selected) {
            case GRID: {
                app.shaderCache["GridShader"]
                    .activate()
                    .setCameraMatrix(app.camera.viewMatrix)
                    .setFrequency(app.getSoundSample(app.getSizeOfGrid()))
                    .renderModel(app.modelCache["GridModel"].preRender())
                    .deactivate()
                break
            }
            case CONT: {
                app.shaderCache["ContShader"]
                    .activate()
                    .setCameraMatrix(app.camera.viewMatrix)
                    .setFrequencyAndTime(app.getSoundSample(32), performance.now())
                    .renderModel(app.modelCache["HistoGridModel"].preRender())
                    .deactivate()
                break
            }
            case POINTS: {
                app.shaderCache["PointShader"]
                    .activate()
                    .setCameraMatrix(app.camera.viewMatrix)
                    .setFrequency(app.getSoundSample(256))
                    .renderModel(app.modelCache["PointModel"].preRender())
                    .deactivate()
                break
            }
            case CUBES: {
                app.shaderCache["CubeShader"]
                    .activate()
                    .setCameraMatrix(app.camera.viewMatrix)
                    .setFrequency(app.getSoundSample(16))
                    .renderModel(app.modelCache["CubeModel"].preRender())
                    .deactivate()
                break
            }
            case CIRCLE: {
                app.shaderCache["CircleShader"]
                    .activate()
                    .setCameraMatrix(app.camera.viewMatrix)
                    .setFrequency(app.getSoundSample())
                    .renderModel(app.modelCache["CircleGrid"].preRender())
                    .deactivate()
                break
            }
            case SPHERE: {
                app.shaderCache["SphereShader"]
                    .activate()
                    .setCameraMatrix(app.camera.viewMatrix)
                    .setFrequencyAndTime(app.getSoundSample(1), performance.now())
                    .renderModel(app.modelCache["SphereModel"].preRender())
                    .deactivate()
                break
            }
        }

        if (skyboxCheck.checked) {
            // We must call setTexture method every time we use texture
            app.shaderCache["SkyMapShader"]
                .activate()
                .setTexture()
                .setCameraMatrix(app.camera.getTranslatelessMatrix())
                .renderModel(app.modelCache["SkyboxModel"])
                .deactivate()
        }
    }

    app.start(onRender)
})