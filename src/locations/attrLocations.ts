// Vertex shader position
export const ATTR_POSITION_NAME: string = "a_position"
export const ATTR_POSITION_LOC: number = 0
// Normal vertex position
export const ATTR_NORMAL_NAME: string = "a_normal"
export const ATTR_NORMAL_LOC: number = 1
// Texture vertex position
export const ATTR_UV_NAME: string = "a_uv"
export const ATTR_UV_LOC: number = 2