/**
 * Class for loading resources, textures, sound, etc.
 * 
 * @export
 * @class Resources
 */
export class Resources {

    public gl: WebGL2RenderingContext

    private textureCache: Array<any>
    private soundCache: Array<any>

    constructor(gl: WebGL2RenderingContext) {
        this.gl = gl
        this.textureCache = []
        this.soundCache = []
    }

    // Order - RIGHT, LEFT, TOP, BOTTOM, BACK, FRONT
    public loadCubeMap(name: string, imgArray: Array<any>): WebGLTexture {
        if (imgArray.length != 6) return null

        // Cube Constants values increment, so easy to start with right and just add 1 in a loop
        // To make sure the code easier costcs by making imgArray coming into the fucntion to have 
        // The imgaes sorted in the same way the constants are set
        // TEXTURE_CUBE_MAP_POSITIVE_X - Right :: TEXTURE_CUBE_MAP_NEGATIVE_X - Left
        // TEXTURE_CUBE_MAP_POSITIVE_Y - Top :: TEXTURE_CUBE_MAP_NEGATIVE_Y - Bottom
        // TEXTURE_CUBE_MAP_POSITIVE_Z - Back :: TEXTURE_CUBE_MAP_NEGATIVE_Z

        let tex: WebGLTexture = this.gl.createTexture()
        this.gl.bindTexture(this.gl.TEXTURE_CUBE_MAP, tex)

        // Pushing images into GPU, if the order is right we can just increment
        for (let i = 0; i < 6; i++) {
            this.gl.texImage2D(this.gl.TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, this.gl.RGBA, this.gl.RGBA, this.gl.UNSIGNED_BYTE, imgArray[i])
        }

        // Scaling
        this.gl.texParameteri(this.gl.TEXTURE_CUBE_MAP, this.gl.TEXTURE_MAG_FILTER, this.gl.LINEAR)
        // Downscaling
        this.gl.texParameteri(this.gl.TEXTURE_CUBE_MAP, this.gl.TEXTURE_MIN_FILTER, this.gl.LINEAR)
        // Strech image to X position (Clamping means stretching all the way to the edge)
        this.gl.texParameteri(this.gl.TEXTURE_CUBE_MAP, this.gl.TEXTURE_WRAP_S, this.gl.CLAMP_TO_EDGE)
        // Strech image to Y position
        this.gl.texParameteri(this.gl.TEXTURE_CUBE_MAP, this.gl.TEXTURE_WRAP_T, this.gl.CLAMP_TO_EDGE)
        // Strech image to Z position
        this.gl.texParameteri(this.gl.TEXTURE_CUBE_MAP, this.gl.TEXTURE_WRAP_R, this.gl.CLAMP_TO_EDGE)

        this.gl.bindTexture(this.gl.TEXTURE_CUBE_MAP, null)

        // Cache texture for future use
        this.textureCache[name] = tex

        return tex
    }

    public loadTexture(name: string, img: any, doYFlip: boolean = false): WebGLTexture {

        let tex: WebGLTexture = this.gl.createTexture();

        // Flip the texture by Y position
        if (doYFlip == true) this.gl.pixelStorei(this.gl.UNPACK_FLIP_Y_WEBGL, true)

        // Bind texture buffer
        this.gl.bindTexture(this.gl.TEXTURE_2D, tex)
        // Push image to GPU
        this.gl.texImage2D(this.gl.TEXTURE_2D, 0, this.gl.RGBA, this.gl.RGBA, this.gl.UNSIGNED_BYTE, img)

        // Scaling
        this.gl.texParameteri(this.gl.TEXTURE_2D, this.gl.TEXTURE_MAG_FILTER, this.gl.LINEAR)
        // Downscaling
        this.gl.texParameteri(this.gl.TEXTURE_2D, this.gl.TEXTURE_MIN_FILTER, this.gl.LINEAR_MIPMAP_NEAREST)
        // // Setup MIPMAPING for better quality rendering (calculates different sizes of texture)
        this.gl.generateMipmap(this.gl.TEXTURE_2D)


        // Clean up
        this.gl.bindTexture(this.gl.TEXTURE_2D, null)

        // Cache texture for future use
        this.textureCache[name] = tex

        // Stop fliping textures
        if (doYFlip == true) this.gl.pixelStorei(this.gl.UNPACK_FLIP_Y_WEBGL, false)

        return tex
    }

    //-------------------------------------
    // GETTERS
    //-------------------------------------
    public getTexture(name: string): WebGLTexture {
        return this.textureCache[name]
    }

    public getSound(name: string): any {
        return this.soundCache[name]
    }


}