/**
 *  Simple utility class
 */
export class Utils {

    static convertFFT(freqArray: Float32Array, sizeOfArray: number): Float32Array {
        const sizeToCrop: number = freqArray.length / sizeOfArray
        let newFreq: Array<number> = []
        for (let i = 0; i < freqArray.length; i += sizeToCrop) {
            const value = (freqArray.slice(i, i + sizeToCrop).reduce((acc, val) => acc += val, 0) / sizeToCrop)
            newFreq.push(value)
        }
        return new Float32Array(newFreq)
    }

}