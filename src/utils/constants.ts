// Basic constants
export const DEG2RAD: number = Math.PI / 180

// Camera constants
export const STEP: number = 0.5

// Model constants
export const GRID: string = "GRID"
export const CIRCLE: string = "CIRCLE"
export const CUBES: string = "CUBES"
export const SPHERE: string = "SPHERE"
export const POINTS: string = "POINTS"
export const CONT: string = "CONT"