// ALL CREDITS FOR THIS CODE GOES TO http://codetheory.in/controlling-the-frame-rate-with-requestanimationframe/

export class RenderLoop {

    // The time in Miliseconds of the last frame
    private msLastFrame: any
    //What function to call for each frame
    private callBack: (deltaTime: number) => void
    // Controll the On/Off state of the render loop
    private isActive: boolean
    // Save the value of how fast the loop is going
    private fps: number
    // Calc how many millisecond per frame in on second of time
    private msFpsLimit: number
    private run: () => void

    constructor(callback: (deltaTime: number) => void = () => { }, fps: number = 60) {
        this.msLastFrame = null
        this.callBack = callback
        this.fps = 0
        this.isActive = false
        this.msFpsLimit = 1000 / fps

        // Build a run method that limits the framerate
        if (fps != undefined && fps > 0) {
            this.run = () => {
                // Gives you the whole number of how many millisecons since the dawn of time
                let msCurrent = performance.now(),
                    msDelta = (msCurrent - this.msLastFrame),
                    deltaTime = msDelta / 1000.0

                if (msDelta >= this.msFpsLimit) {
                    this.fps = Math.floor(1 / deltaTime)
                    this.msLastFrame = msCurrent
                    this.callBack(deltaTime)
                }

                if (this.isActive) window.requestAnimationFrame(this.run)
            }
        } else {
            this.run = () => {
                let msCurrent = performance.now(),
                    deltaTime = (msCurrent - this.msLastFrame) / 1000.0

                this.fps = Math.floor(1 / deltaTime)
                this.msLastFrame = msCurrent

                this.callBack(deltaTime)
                if (this.isActive) window.requestAnimationFrame(this.run)
            }
        }
    }

    public setCallback(onRender: (deltaTime: number) => void): RenderLoop {
        this.callBack = onRender
        return this
    }

    public setFPS(fps: number): RenderLoop {
        this.msFpsLimit = 1000 / fps
        return this
    }

    public start(): RenderLoop {
        this.isActive = true
        this.msLastFrame = performance.now()
        window.requestAnimationFrame(this.run)
        return this
    }

    public stop(): RenderLoop {
        this.isActive = false
        return this
    }

}