import { Sound } from "./Sound";

let filterSample = {
    FREQ_MUL: 7000,
    QUAL_MUL: 30,
    playing: false
}

/**
 * Class controlling sound filters
 * 
 * @export
 * @class SoundController
 */
export class SoundController {

    private playButton: HTMLButtonElement
    private stopButton: HTMLButtonElement
    private volumeInput: HTMLInputElement
    private fileUpload: HTMLInputElement
    private resetButton: HTMLButtonElement

    private qualityInput: HTMLInputElement
    private lowPassInput: HTMLInputElement
    private highPassInput: HTMLInputElement
    private bassInput: HTMLInputElement
    private trembleInnput: HTMLInputElement
    private midInput: HTMLInputElement

    public sound: Sound

    constructor(sound: Sound) {
        this.sound = sound

        this.playButton = document.getElementById("playPause") as HTMLButtonElement
        this.stopButton = document.getElementById("stopButton") as HTMLButtonElement
        this.resetButton = document.getElementById("resetButton") as HTMLButtonElement
        this.fileUpload = document.getElementById("soundFile") as HTMLInputElement

        this.volumeInput = document.getElementById("volumeInput") as HTMLInputElement

        this.lowPassInput = document.getElementById("lowPassInput") as HTMLInputElement
        this.highPassInput = document.getElementById("highPassInput") as HTMLInputElement
        this.qualityInput = document.getElementById("qualityInput") as HTMLInputElement
        this.bassInput = document.getElementById("bassInput") as HTMLInputElement
        this.trembleInnput = document.getElementById("trembleInput") as HTMLInputElement
        this.midInput = document.getElementById("midInput") as HTMLInputElement

        this.resetButton.addEventListener("click", () => {
            this.lowPassInput.value = "1"
            this.qualityInput.value = "1"
            this.bassInput.value = "0"
            this.trembleInnput.value = "0"
            this.midInput.value = "0"
            this.init()
        })

        this.playButton.addEventListener("click", () => {
            this.sound.audio.play()
        })
        this.stopButton.addEventListener("click", () => {
            this.sound.audio.pause()
        })

        this.volumeInput.addEventListener("input", this.changeVolume.bind(this))
        this.lowPassInput.addEventListener("input", this.reduceHighFrequencies.bind(this))
        this.qualityInput.addEventListener("input", this.changeQuality.bind(this))
        this.fileUpload.addEventListener("change", this.changeSrc.bind(this))
        this.bassInput.addEventListener("input", this.changeBass.bind(this))
        this.trembleInnput.addEventListener("input", this.changeTremble.bind(this))
        this.midInput.addEventListener("input", this.changeMid.bind(this))
        this.highPassInput.addEventListener("input", this.reduceLowFrequencies.bind(this))

        this.init()
    }

    private init(): SoundController {
        // Initial config
        this.reduceHighFrequencies()
            .changeVolume()
            .changeBass()
            .changeTremble()
            .changeQuality()
            .changeMid()
        return this
    }

    private changeBass(): SoundController {
        this.sound.bassFilter.gain.setValueAtTime(parseFloat(this.bassInput.value), this.sound.audioContext.currentTime)
        return this
    }

    private changeTremble(): SoundController {
        this.sound.trembleFilter.gain.setValueAtTime(parseFloat(this.trembleInnput.value), this.sound.audioContext.currentTime)
        return this
    }

    private changeMid(): SoundController {
        this.sound.midFilter.gain.setValueAtTime(parseFloat(this.midInput.value), this.sound.audioContext.currentTime)
        return this
    }

    private changeVolume(): SoundController {
        let fraction: number = parseInt(this.volumeInput.value) / parseInt(this.volumeInput.max)
        this.sound.gainNode.gain.setValueAtTime(fraction * fraction, this.sound.audioContext.currentTime)
        return this
    }

    private reduceHighFrequencies(): SoundController {
        // Clamp the frequency between the minimum value (40 Hz) and half of the sampling rate
        let minValue: number = 40
        let maxValue: number = this.sound.audioContext.sampleRate / 2
        // Logarithm (base 2) to compute how many octaves on an exponential scale.
        let numberOfOctaves: number = Math.log(maxValue / minValue) / Math.LN2
        // Compute multiplier from 0 to 1 based on exponential scale
        let multiplier: number = Math.pow(2, numberOfOctaves * (parseFloat(this.lowPassInput.value) - 1.0))
        // Get back to the frequency value between min and max
        this.sound.lowPassFilter.frequency.setValueAtTime(maxValue * multiplier, this.sound.audioContext.currentTime)
        return this
    }

    private reduceLowFrequencies(): SoundController {
        let maxValue: number = this.sound.audioContext.sampleRate / 2
        this.sound.highPassFilter.frequency.setValueAtTime(maxValue * parseFloat(this.highPassInput.value), this.sound.audioContext.currentTime)
        return this
    }

    private changeQuality(): SoundController {
        // Q represent quality factor, in 'lowpass' algorithm it indicated how the peaked frequency is around the cutoff,
        // the grater the value is, the greater is the peak
        this.sound.lowPassFilter.Q.setValueAtTime(this.qualityInput.value as any * filterSample.QUAL_MUL, this.sound.audioContext.currentTime)
        return this
    }

    private changeSrc(): SoundController {
        // Create relative path as BLOB for given file
        this.sound.audio.src = URL.createObjectURL(this.fileUpload.files[0])
        this.sound.audio.play()
        return this
    }
}