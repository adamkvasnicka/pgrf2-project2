export const HSL_TO_RGB = `

// HSL to RGB code inspired by
// http://www.niwa.nu/2013/05/math-behind-colorspace-conversions-rgb-hsl/

float hue2rgb(float t1, float t2, float hue)
{
  if (hue < 0.0) hue += 1.0;
  if (hue > 1.0) hue -= 1.0;
  if ((6.0 * hue) < 1.0) return t1 + (t2 - t1) * 6.0 * hue;
  if ((2.0 * hue) < 1.0) return t2;
  if ((3.0 * hue) < 2.0) return t1 + (t2 - t1) * (2.0/3.0 - hue) * 6.0;
  return t1;
}

vec3 hsl2rgb(float height)
{
  // Basic values
  float luminance = 0.5;
  float saturation = 1.0;

  float r, g, b, temp1, temp2;

  if(luminance < 0.5) {
    temp1 = luminance * (1.0 + saturation); 
  } else if (luminance > 0.5) {
    temp1 = luminance + saturation - luminance * saturation;
  }

  temp2 = 2.0 * luminance - temp1;
 
  r = hue2rgb(temp1, temp2, height + (1.0/3.0));
  g = hue2rgb(temp1, temp2, height);
  b = hue2rgb(temp1, temp2, height - (1.0/3.0));

  return vec3(r, g, b);
}
`