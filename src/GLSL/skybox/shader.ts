export const SKY_VERTEX = `#version 300 es
in vec4 a_position;
in vec2 a_uv;

uniform mat4 uMVMatrix;
uniform mat4 uPMatrix;
uniform mat4 uCameraMatrix;

out highp vec3 texCoord; // Interpolate UV values to the fragment shader

void main(void){
    texCoord = a_position.xyz;
    gl_Position = uPMatrix * uCameraMatrix * uMVMatrix * vec4(a_position.xyz,1.0);
}`

export const SKY_FRAGMENT = `#version 300 es
precision mediump float;

in highp vec3 texCoord;
uniform samplerCube uSkyTex;

out vec4 finalColor;
void main(void) { 
    finalColor = texture(uSkyTex,texCoord);
}
`