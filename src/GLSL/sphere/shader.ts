import { HSL_TO_RGB } from "../utility/color"

export const SPHERE_VERTEX: string = `#version 300 es
in vec4 a_position; // Standart position data.. Making it vec4, the W component is used as color index from uColor
in vec3 a_normal;

uniform mat4 uMVMatrix;
uniform mat4 uPMatrix;
uniform mat4 uCameraMatrix;

uniform mediump float uSoundFreq[1024];

${HSL_TO_RGB}

out lowp vec4 color;

void main(void){

    int defaultFactor = 30;

    float scaleX = a_normal.x + uSoundFreq[int(gl_VertexID)];
    float scaleY = a_normal.y + uSoundFreq[int(gl_VertexID)];
    float scaleZ = a_normal.z + uSoundFreq[int(gl_VertexID)];

    float scaleBySound = uSoundFreq[int(gl_VertexID / defaultFactor)];

    mat4 soundFreq = mat4(
        vec4(scaleBySound,0,0,0),
        vec4(0,1,0,0),
        vec4(0,0,1,0),
        vec4(0,0,0,1)
    );

    vec4 P = vec4(a_position.xyz, 1.0) * soundFreq;

    color = vec4(hsl2rgb(abs(P.x)),1.0);

    gl_PointSize = 10.0;
    gl_Position = uPMatrix * uCameraMatrix * uMVMatrix * P;
}`

export const SPHERE_FRAGMENT: string = `#version 300 es
precision mediump float;

in lowp vec4 color;

out vec4 finalColor;

void main(void) {
    finalColor = color;
}
`