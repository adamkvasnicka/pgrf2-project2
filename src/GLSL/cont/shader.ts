import { HSL_TO_RGB } from "../utility/color"

export const CONT_VERTEX: string = `#version 300 es
layout(location=0) in vec3 a_position; // Standart position data

uniform mat4 uMVMatrix;
uniform mat4 uPMatrix;
uniform mat4 uCameraMatrix;

uniform mediump float uSoundFreq[2048];

${HSL_TO_RGB}

out vec4 color;

void main(void){

    vec3 P = vec3(a_position.x, a_position.y + uSoundFreq[int(gl_VertexID)], a_position.z);

    vec3 huecolor = hsl2rgb(P.y);
    if(gl_VertexID < 32) {
      color = vec4(1.0, 1.0, 1.0, 1.0);
    } else {
      color = vec4(huecolor, 1.0);
    }

    gl_Position = uPMatrix * uCameraMatrix * uMVMatrix * vec4(P, 1.0);
}
`

export const CONT_FRAGMENT: string = `#version 300 es
precision mediump float;

in vec4 color;

out vec4 finalColor;

void main(void) {
    finalColor = color;
}
`