import { HSL_TO_RGB } from "../utility/color"

export const CIRCLE_VERTEX: string = `#version 300 es
layout(location=0) in vec3 a_position; // Standart position data

uniform mat4 uMVMatrix;
uniform mat4 uPMatrix;
uniform mat4 uCameraMatrix;

uniform mediump float uSoundFreq[2048];
uniform float radius;

${HSL_TO_RGB}

out vec4 color;

void main(void){

    vec3 P = vec3(a_position.x, a_position.y + (2.0 * uSoundFreq[int(gl_VertexID)]) , a_position.z);

    color = vec4(hsl2rgb(P.y),1.0);

    mat4 scaleByRadius = mat4(
      vec4(radius,0,0,0),
      vec4(0,1,0,0),
      vec4(0,0,radius,0),
      vec4(0,0,0,1)
    );

    gl_Position = uPMatrix * uCameraMatrix * uMVMatrix * scaleByRadius * vec4(P, 1.0);
}
`

export const CIRCLE_FRAGMENT: string = `#version 300 es
precision mediump float;

in vec4 color;

out vec4 finalColor;

void main(void) {
    finalColor = color;
}
`