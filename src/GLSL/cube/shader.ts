import { HSL_TO_RGB } from "../utility/color"

export const CUBE_VERTEX: string = `#version 300 es
in vec4 a_position;

uniform mat4 uMVMatrix;
uniform mat4 uPMatrix;
uniform mat4 uCameraMatrix;

uniform mediump float uSoundFreq[512];

${HSL_TO_RGB}

out lowp vec4 color;

void main(void){

    mat4 soundTransform = mat4(
        1.0,0,0,0,
        0,uSoundFreq[int(gl_VertexID / 8)],0,0,
        0,0,1.0,0,
        0,0,0,1.0
    );

    vec4 P = soundTransform * vec4(a_position.x, a_position.y, a_position.z, 1.0);

    color = vec4(hsl2rgb(P.y),1.0);

    gl_Position = uPMatrix * uCameraMatrix * uMVMatrix * P;
}`

export const CUBE_FRAGMENT: string = `#version 300 es
precision mediump float;

in lowp vec4 color;

out vec4 finalColor;

void main(void) {
    finalColor = color;
}
`