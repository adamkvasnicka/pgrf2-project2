import { Transform } from "./Transformer"
import { VAO } from "./interfaces/vao";

/**
 * Class representing 3D model
 * 
 * @export
 * @class Model
 */
export class Model {

    public mesh: VAO
    public transform: Transform

    constructor(mesh: VAO) {

        this.mesh = mesh

        this.transform = new Transform()
    }

    //------------------------------------
    // Getters/Setters
    public setScale(x: number, y: number, z: number): Model {
        this.transform.setScale(x, y, z)
        return this
    }
    public setPosition(x: number, y: number, z: number): Model {
        this.transform.setPosition(x, y, z)
        return this
    }
    public setRotation(x: number, y: number, z: number): Model {
        this.transform.setRotation(x, y, z)
        return this
    }

    public addScale(x: number, y: number, z: number): Model {
        this.transform.addScale(x, y, z)
        return this
    }
    public addPosition(x: number, y: number, z: number): Model {
        this.transform.addPosition(x, y, z)
        return this
    }
    public addRotation(x: number, y: number, z: number): Model {
        this.transform.addRotation(x, y, z)
        return this
    }

    // check if there is change in our model transformation matrix
    public preRender(): Model {
        this.transform.updateMatrix()
        return this
    }

}