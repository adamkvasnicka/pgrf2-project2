/**
 * This is how our attribute location object must look like
 * 
 * @export
 * @interface AttribLocation
 */
export interface AttribLocation {
    position: number,
    normal: number,
    uv: number
}

/**
 * This is how our uniform location object must look like
 * 
 * @export
 * @interface UniformLocation
 */
export interface UniformLocation {
    perspectiveMatrix: any,
    modelMatrix: any,
    cameraMatrix: any,
    mainTexture: any
}