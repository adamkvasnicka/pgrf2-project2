/**
 * Mesh VAO interface
 * 
 * @export
 * @interface VAO
 */
export interface VAO {
    drawMode: number,
    vao?: WebGLBuffer,
    bufVertices?: WebGLBuffer,
    vertexComponentLen?: number,
    vertexCount?: number,
    bufNormals?: WebGLBuffer,
    bufUVs?: WebGLBuffer,
    bufIndices?: WebGLBuffer,
    indexCount?: number,
    noCulling?: boolean
}